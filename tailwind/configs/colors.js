module.exports = {
  dark: {
    0: '#0F1827',
    1: '#1F2C41',
    2: '#1e2b42'
  },
  yellow: '#F3F80C',
  gray: {
    0:'#C3C4C7',
    1:'#B1B3B7',
    2:'#10192729',
    3:'#FAFAFA',
    4: '#E4E5E8'
  },
  blue: {
    0:'#5372A3',
    1: '#0F1827',
    2: '#1F2C41',
    3: '#0A2458',
    4:'#5372A3',
    'bright': {
      DEFAULT: '#2A70FF',
      1: '#2A70FF',
      2: '#0040C3',
    }
  },
  white: {
    DEFAULT: 'white',
    0: '#FAFAFA',
    1: '#E4E5E4'
  }
}
