module.exports = {
  'Druk': 'Druk',
  'Grotesk-Bold': 'Grotesk-Bold',
  'Grotesk-Mono-Medium': 'Grotesk-Mono-Medium',
  'Grotesk-Text-Medium': 'Grotesk-Text-Medium',
  'Grotesk-Text-Regular': 'Grotesk-Text-Regular',
  'Reckless-Medium': 'Reckless-Medium'
}
